import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/login")
public class ControllerServlet extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String un=request.getParameter("inputUsername");
        String pw=request.getParameter("inputPassword");
        response.setContentType("text/html");
        HttpSession session = request.getSession();
        if(un.equals("system") && pw.equals("java"))
        {
            request.setAttribute("inputUsername", un);
            RequestDispatcher rd = request.getRequestDispatcher("/page1.jsp");
            rd.forward(request, response);
            response.sendRedirect("page1.jsp");
            return;
        }
        else
        {
            session.invalidate();
            request.setAttribute("errorMessage", "Invalid user or password! Please try again!");
            RequestDispatcher rd = request.getRequestDispatcher("/index.jsp");
            rd.forward(request, response);
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }
}
