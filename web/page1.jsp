<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Page 2</title>
</head>
<body>
<%@ include file="header.jsp" %>

<%@ include file="footer.jsp" %>
<div id="wrapper">
    <%@ include file="sidebar.jsp" %>
    <div id="content-wrapper">
    <div class="container-fluid">
        <h1 align="center">WELCOME TO MY HOME PAGE, <%= request.getParameter("inputUsername")%></h1>
    </div>
</div>
</div>
</body>
</html>
